#include "TorchBinder.h"
#include <torch/script.h>
#include <torch/nn/functional/activation.h>
#include <iostream>

TorchBinder::TorchBinder(void) {
    device = torch::kCPU;
}

TorchBinder::~TorchBinder(void) {

}

void TorchBinder::loadModel(std::string& modelPath) {
    if (torch::cuda::is_available()) {
        device = torch::kCUDA;
        std::cout << "torch cuda!!" << std::endl;
    } else {
        device = torch::kCPU;
        std::cout << "shit.. set cpu!!" << std::endl;
    }
    
    std::cout << "Trying to load model.." << std::endl;
    try {
        torchModel = torch::jit::load(modelPath, device);
        torchModel.eval();
        std::cout << "AI model loaded successfully" << std::endl;
    } catch (const c10::Error& e) {
        std::cerr << e.what() << std::endl;
    }
    // TODO: Add warm up code
}

void TorchBinder::normalizeTensor(torch::Tensor& tensor) {
    // order: RGB channel
    const float mean[3] = { 0.485, 0.456, 0.406 };
    const float std[3] = { 0.229, 0.224, 0.225 };

    for (int32_t channel = 0; channel < 3; channel++) {
        tensor[channel].sub_(mean[channel]).div_(std[channel]);
    }
}

torch::Tensor TorchBinder::preProcess(cv::Mat& inputFrame, cv::Size inputSize) {
    cv::Mat resizedMat;
    cv::Mat cvtMat;
    torch::Tensor inputTensor;

    cv::resize(inputFrame, resizedMat, inputSize);
    resizedMat.convertTo(cvtMat, CV_32FC3, 1.f / 255.f);
    inputTensor = torch::from_blob(cvtMat.data,{cvtMat.rows, cvtMat.cols, cvtMat.channels()}).permute({2,0,1});

    normalizeTensor(inputTensor);
    std::cout << "after norm tensor size:  " << inputTensor.sizes() << std::endl;
    
    inputTensor = inputTensor.unsqueeze_(0).to(device);
    return inputTensor;
}

c10::IValue TorchBinder::inference(torch::Tensor& inputTensor) {
	torch::NoGradGuard noGrad;
	torchModel.eval();
	c10::IValue outputIValue = torchModel.forward({inputTensor});
	torch::cuda::synchronize();
	return outputIValue;
}

torch::Tensor TorchBinder::postProcess(c10::IValue& outputIValue) {
	torch::Tensor outputProbTensor = torch::nn::functional::softmax(outputIValue.toTensor(), torch::nn::functional::SoftmaxFuncOptions(1));
	if (device == torch::kCUDA) {
		outputProbTensor = outputProbTensor.to(torch::kCPU);
	}

	return outputProbTensor;
}

