#ifndef __TORCH_BINDER_H__
#define __TORCH_BINDER_H__

#include <string>
#include <opencv2/opencv.hpp>
#include <torch/torch.h>

class TorchBinder {
public:
    TorchBinder(void);
    ~TorchBinder(void);

	void loadModel(std::string& modelPath);
	torch::Tensor preProcess(cv::Mat& inputFrame, cv::Size inputSize);
	c10::IValue inference(torch::Tensor& inputTensor);
	torch::Tensor postProcess(c10::IValue& outputIValue);
    
private:
    void normalizeTensor(torch::Tensor& tensor);

public:
    torch::jit::script::Module torchModel;

private:
    torch::DeviceType device;

};

#endif
