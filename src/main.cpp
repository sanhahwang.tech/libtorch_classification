#include "TorchBinder.h"

int main(void) {
    std::string modelPath = "../weights/effcientnet_b0_script.pt";
    TorchBinder torchBinder = TorchBinder();;
	torchBinder.loadModel(modelPath);

    cv::Mat img = cv::imread("../data/images/bus.jpg");
    cv::Size inputSize(224,224);
	torch::Tensor inputTensor = torchBinder.preProcess(img, inputSize);
	c10::IValue outputIValue = torchBinder.inference(inputTensor);
	torch::Tensor outputProbTensor = torchBinder.postProcess(outputIValue);
    std::cout << "inference end!!!" << std::endl;

    std::tuple<at::Tensor, at::Tensor> top5_tensor = outputProbTensor.topk(5);
    std::cout << "result value: " << std::get<0>(top5_tensor) << std::endl;
    std::cout << "result indices: " << std::get<1>(top5_tensor) << std::endl;

    return 0;
}
